//Session 23 - MongoDB Configuration and CRUD

/*
	CRUD OPERATIONS

		---- CREATE OPERATION ----
		 - add/insert operations add new documents to a collection. 

		syntax:

		db.collection.insertOne(document{});
		db.collection.insertMany([document{},document{}]);

		Example: we will insert one document

		db.users.insertOne(
			{
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"contact": {
					"phone": "87654321",
					"email": "janedoe@gmail.com"
				},
				"courses": ["CSS","Javascript","Python"],
				"department": "none"
			}
		)

		Example: we will insert 2 documents


	db.users.insertMany(
		[
			{
				"firstName": "Stephen",
				"lastName": "Hawking",
				"age": 76,
				"contact": {
					"phone": "87654321",
					"email": "stephenhawking@gmail.com"
				},
				"courses": ["Python", "React", "PHP"],
				"department": "none"
			},
			{
				"firstName": "Neil",
				"lastName": "Armstrong",
				"age": 82,
				"contact": {
					"phone": "87654321",
					"email": "neilarmstrong@gmail.com"
				},
				"courses": ["React", "Laravel", "Sass"],
				"department": "none"
			}
		]
	)

	Read Operation
		- retrieve documents from a collection
		- returns array

	Syntax:
		db.collections.find(query,project)

		query - filter

		db.users.find

	Update Operation
		- modify existing documents in a collection

	Syntax:
		db.collections.updateOne(filter, update, options)
		db.collections.updateMany()
		db.collections.replaceOne()
	*/

	//we will insert this document as an example to the updateOne() method
	db.users.insertOne({
		"firstName": "test",
		"lastName": "test",
		"age": 0,
		"contact": {
			"phone": "0",
			"email": "test"
		},
		"courses": [],
		"department": "test"
	})

	//use the updateOne() method to this example
	db.users.updateOne(
		{//filter
			"firstName": "test"
		},
		{//update
			$set: {
					"firstName": "Bill",
					"lastName": "Gates",
					"age": 65,
					"contact": {
							"phone": "12345678",
							"email": "bill@gmail.com"
					},
					"courses": ["PHP","Laravel","HTML"],
					"department": "Operations",
					"status": "active"
			}
		}
	)

		//sets new value/adding new field
		db.users.updateOne(
			{
				"_id" : ObjectId("621f2c25325f6dbbbc45a9bb"),
			},
			{
				$set: {
					"isAdmin": true
				}
			}
		)

		//remove existing field
		db.users.updateOne(
			{
				"_id" : ObjectId("621f2c25325f6dbbbc45a9bb"),
			},
			{
				$unset: {
					"isAdmin": true
				}
			}
		)

	//UPDATE MULTIPLE DOCUMENTS

	//update department to HR
	db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}
	)

	//add field status
	db.users.updateMany(
        {
                "department" : "HR"
        },
        {
                $set: { 
                    "status" : "active" 
                }
        }
	)

	//if using updateOne method, if you update documents with isAdmin field to false
	  //--this will only update the first document that passes the filter document

	 //updateOne vs replaceOne

	 	//using updateOne method, update the first name
	 	db.users.updateOne(
	 		{
	 			"_id" : ObjectId("62200f587d90ada2b518df75"),
	 		},
	 		{
	 			$unset: {
	 				"firstName": "Carine"
	 			}
	 		}
	 	)
	 		//updates the field/s specified in the update document parameter while keeping all the existing fields

	 	//using replaceOne, update the last name
	 	//replace one removes all other fields except the one indicated in the code
	 	db.users.replaceOne(
	 		{
	 			"_id" : ObjectId("62200f587d90ada2b518df75"),
	 		},
	 		{
	 				"lastName": "Cruz"
	 		}
	 	)
	 		//replace the whole document with the replacement document parameter


	// DELETE METHOD
		//Removes a single document from a collection

		//db.collections.deleteOne({filter})
		//db.collections.deleteMany({filter})

		Syntax:

			db.collections.deleteOne(
				{ "property": "value"}
			)

			db.users.deleteOne({"_id" : ObjectId("62200f587d90ada2b518df75")})
	 	
	 	//Delete Many
	 		//Removes all documents that match the filter from a collection.

	 	//create dummy documents for deleteMany method
	 	db.users.insertMany(
	 		[
	 			{
	 				"course": "JS",
	 				"price": 1000
	 			},
	 			{
	 				"course": "JAVA",
	 				"price": 3000
	 			},
	 			{
	 				"course": "PHP",
	 				"price": 5000
	 			},
	 		]
	 	)

	 	//Query Operators
	 	//{ field: { $gte: value } }

	 	//use $in operator to target multiple documents of the same field
	 	db.users.deleteMany(
	 		{ "price": { $gte: 3000 } }
	 	)

	 	db.users.deleteMany(
	 		{
	 			"_id": {$in: [ObjectId("id"),
	 						ObjectId("id"),
	 						ObjectId("id")
	 		]}
	 		}
	 	)